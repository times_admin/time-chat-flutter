import 'package:flutter/material.dart';
import 'package:myapp/pages/chat.dart';
import 'package:myapp/pages/main_page.dart';
import 'package:myapp/pages/new_route.dart';
import 'package:myapp/pages/echo_route.dart';
import 'package:myapp/app_theme.dart';
import 'package:myapp/pages/tab_bar_demo.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  static const String _title = 'DEV';

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: _title,
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
        textTheme: AppTheme.textTheme,
        platform: TargetPlatform.iOS,
      ),
      //注册路由表
      routes: {
        "/": (context) => const Main(),
        "/new_page": (context) => const NewRoute(),
        "/new_page2": (context) => const EchoRoute(),
        "/tabbar": (context) => const TabBarDemo(),
        "/chat": (context) => const Chat(),
      },
    );
  }
}
