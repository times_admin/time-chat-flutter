import 'dart:developer';

import 'package:flui_nullsafety/flui_nullsafety.dart';
import 'package:flutter/material.dart';

class My extends StatefulWidget {
  const My({Key? key}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<My> {
  TextEditingController _textController = TextEditingController();

  @override
  void initState() {
    super.initState();
    _textController.addListener(() {
      // log(_textController.text);
    });
  }

  void send() {
    log(_textController.text);
    _textController.text = "";
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text('My'),
          ),
        ],
      ),
    );
  }
}
