// import 'package:flui/flui.dart';
import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:myapp/widget/dev.dart';

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: const DevStatelessWidget(),
    );
  }
}
