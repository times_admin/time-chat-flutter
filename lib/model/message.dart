import 'dart:convert';

MessageModel messageModelFromJson(String str) =>
    MessageModel.fromJson(json.decode(str));

String messageModelToJson(MessageModel data) => json.encode(data.toJson());

class MessageModel {
  String type;
  String name;
  String msg;
  String dateTime;

  MessageModel({
    required this.type,
    required this.name,
    required this.msg,
    required this.dateTime,
  });

  factory MessageModel.fromJson(Map<String, dynamic> json) => MessageModel(
        type: json["type"],
        name: json["name"],
        msg: json["msg"],
        dateTime: json["dateTime"],
      );

  Map<String, dynamic> toJson() => {
        "type": type,
        "name": name,
        "msg": msg,
        "dateTime": dateTime,
      };
}
