import 'dart:developer';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:dio/dio.dart';

// 新页面
class NewRoute extends StatelessWidget {
  const NewRoute({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("New route"),
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          children: <Widget>[
            Expanded(
              flex: 0,
              child: Text('test'),
            ),
            Expanded(
              flex: 10,
              child: ListView.builder(
                itemCount: 5,
                itemExtent: 50.0, //强制高度为50.0
                itemBuilder: (BuildContext context, int index) {
                  return Card(
                    child: ListTile(
                      title: Text("$index"),
                    ),
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class TestWidget extends StatelessWidget {
  const TestWidget({Key? key}) : super(key: key);

  void apiTest() async {
    var dio = Dio();
    Response response = await dio.get(
        "http://172.16.1.71:7000/api/list/1?path=%2Fweb-pc-convergence-platform&password=&orderBy=&orderDirection=");
    var res = response.data;
    log(res);
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: CupertinoButton(
        padding: const EdgeInsets.all(10),
        color: CupertinoColors.activeBlue,
        child: const Text('TestWidget'),
        onPressed: () {
          apiTest();
        },
      ),
    );
  }
}
