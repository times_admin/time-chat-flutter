// import 'package:flui/flui.dart';
import 'dart:async';
import 'dart:developer';

import 'package:flui_nullsafety/flui_nullsafety.dart';
import 'package:flutter/material.dart';
import 'package:myapp/app_theme.dart';
import 'package:myapp/widget/message.dart';
import 'package:myapp/model/message.dart';

class Chat extends StatefulWidget {
  const Chat({Key? key}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Chat> {
  ScrollController _scrollController = ScrollController();
  TextEditingController _textController = TextEditingController();
  List list = [
    {
      "type": 'left',
      "name": '王二',
      "msg": '你好，我是老王',
      "dateTime": '2022/1/22 00:00:00',
    },
    {
      "type": 'right',
      "name": '时光',
      "msg": '你好，我是时光',
      "dateTime": '2022/1/22 00:00:00',
    },
  ];
  late Widget _messageList;
  late FocusNode myFocusNode;

  @override
  void initState() {
    super.initState();
    myFocusNode = FocusNode();

    log(list.length.toString());
    _messageList = buildList();
  }

  @override
  void didUpdateWidget(covariant Chat oldWidget) {
    super.didUpdateWidget(oldWidget);
    log('didUpdateWidget');
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    log('didChangeDependencies');
  }

  @override
  void dispose() {
    myFocusNode.dispose();
    super.dispose();
  }

  void send() {
    if (_textController.text.isEmpty) return;
    list.add({
      "type": 'right',
      "name": '时光',
      "msg": _textController.text,
      "dateTime": '2022/1/22 00:00:00',
    });
    setState(() {
      _messageList = buildList();
    });
    log(_textController.text);
    _textController.clear();
    Timer(const Duration(milliseconds: 500), () {
      log('滚动到底部');
      _scrollController.animateTo(
        _scrollController.position.maxScrollExtent,
        curve: Curves.easeOut,
        duration: const Duration(milliseconds: 100),
      );
    });
    myFocusNode.requestFocus();
  }

  Widget buildList() {
    List<Widget> tiles = [];
    for (var item in list) {
      MessageModel m = MessageModel.fromJson(item);
      tiles.add(
        Message(
          type: m.type,
          name: m.name,
          msg: m.msg,
        ),
      );
    }
    Widget content = ListView(
      controller: _scrollController,
      children: [...tiles],
    );
    return content;
  }

  @override
  Widget build(BuildContext context) {
    log('page 刷新');
    return Scaffold(
      backgroundColor: AppTheme.notWhite,
      appBar: AppBar(title: Text('Chat')),
      body: Padding(
        padding: const EdgeInsets.all(0.0),
        child: Column(
          children: [
            Expanded(
              flex: 1,
              child: _messageList,
            ),
            Expanded(
              flex: 0,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  children: <Widget>[
                    Expanded(
                      flex: 1,
                      child: Padding(
                        padding: const EdgeInsets.all(0.0),
                        child: TextField(
                          autofocus: true,
                          focusNode: myFocusNode,
                          textInputAction: TextInputAction.done,
                          controller: _textController,
                          onSubmitted: (value) {
                            send();
                          },
                          decoration: const InputDecoration(
                            // labelText: "用户名",
                            hintText: "输入消息",
                            // prefixIcon: Icon(Icons.person),
                          ),
                          onChanged: (value) => {
                            // log(value),
                          },
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(0.0),
                      child: FLRaisedButton(
                        onPressed: () {
                          send();
                        },
                        child: const Text('发送'),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
