import 'package:flutter/material.dart';
import 'package:myapp/app_theme.dart';
import 'package:myapp/view/home.dart';
import 'package:myapp/view/my.dart';

class Main extends StatefulWidget {
  const Main({Key? key}) : super(key: key);

  @override
  _MainState createState() => _MainState();
}

class _MainState extends State<Main> {
  String title = '首页';

  int _currentIndex = 0;

  Widget _currBody = const Home();

  void _onTap(int index) {
    switch (index) {
      case 0:
        _currBody = const Home();
        title = '首页';
        break;
      case 1:
        _currBody = const My();
        title = '我的';
        break;
    }
    setState(() {
      _currentIndex = index;
    });
    // debugPrint(_currentIndex.toString());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppTheme.notWhite,
      bottomNavigationBar: BottomNavigationBar(
        onTap: _onTap,
        type: BottomNavigationBarType.shifting,
        selectedItemColor: Theme.of(context).primaryColor,
        unselectedItemColor: Colors.black,
        currentIndex: _currentIndex,
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            title: Text('首页'),
            icon: Icon(Icons.home),
          ),
          BottomNavigationBarItem(
            title: Text('我的'),
            icon: Icon(Icons.perm_identity),
          ),
        ],
      ),
      appBar: AppBar(title: Text(title)),
      body: _currBody,
    );
  }
}
