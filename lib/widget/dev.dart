import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:myapp/pages/tip_toute.dart';

// Dev 小部件
// 按钮

class DevStatelessWidget extends StatelessWidget {
  const DevStatelessWidget({Key? key}) : super(key: key);

// run in somewhere

  static bool _checkValue = true;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Padding(
        padding: const EdgeInsets.all(18),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                CupertinoButton(
                  padding: const EdgeInsets.all(10),
                  color: CupertinoColors.activeBlue,
                  child: const Text('NewRoute'),
                  onPressed: () {
                    debugPrint('NewRoute');
                    Navigator.pushNamed(context, "/new_page");
                    // Navigator.push(
                    //   context,
                    //   MaterialPageRoute(builder: (context) {
                    //     return const NewRoute();
                    //   }),
                    // );
                  },
                ),
                CupertinoButton(
                  padding: const EdgeInsets.all(10),
                  color: CupertinoColors.activeBlue,
                  child: const Text('EchoRoute'),
                  onPressed: () {
                    debugPrint('EchoRoute');
                    Navigator.of(context)
                        .pushNamed("/new_page2", arguments: "你好，我是DEV");
                  },
                ),
                CupertinoButton(
                  padding: const EdgeInsets.all(10),
                  color: CupertinoColors.activeBlue,
                  child: const Text('TipRoute'),
                  onPressed: () async {
                    // 打开`TipRoute`，并等待返回结果
                    var result = await Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) {
                          return const TipRoute(
                            // 路由参数
                            text: "我是DEV",
                          );
                        },
                      ),
                    );
                    //输出`TipRoute`路由返回结果
                    debugPrint("路由返回值: $result");
                  },
                ),
                CupertinoButton(
                  padding: const EdgeInsets.all(10),
                  color: CupertinoColors.activeBlue,
                  child: const Text('tabbar'),
                  onPressed: () {
                    debugPrint('tabbar');
                    // Navigator.pushNamed(context, "/tabbar");
                    showCupertinoModalPopup(
                      context: context,
                      builder: (BuildContext context) {
                        return CupertinoActionSheet(
                          title: const Text('提示'),
                          message: const Text('是否要删除当前项？'),
                          actions: <Widget>[
                            CupertinoActionSheetAction(
                              child: const Text('删除'),
                              onPressed: () {},
                              isDefaultAction: true,
                            ),
                            CupertinoActionSheetAction(
                              child: const Text('暂时不删'),
                              onPressed: () {},
                              isDestructiveAction: true,
                            ),
                          ],
                        );
                      },
                    );
                  },
                ),
              ],
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 8.0),
              child: Row(
                children: [
                  CupertinoButton(
                    padding: const EdgeInsets.all(10),
                    color: CupertinoColors.activeBlue,
                    child: const Text('chat'),
                    onPressed: () {
                      debugPrint('chat');
                      Navigator.pushNamed(context, "/chat");
                    },
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}

class MyStatelessWidget extends StatelessWidget {
  const MyStatelessWidget({Key? key}) : super(key: key);

  static int _count = 0;

  void add() {
    _count++;
    debugPrint(_count.toString());
  }

  @override
  Widget build(BuildContext context) {
    return CupertinoButton(
      color: CupertinoColors.systemGreen,
      onPressed: () {
        add();
      },
      child: const Text('Click Me'),
    );
  }
}
