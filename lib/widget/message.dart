import 'dart:developer';

import 'package:flui_nullsafety/flui_nullsafety.dart';
import 'package:flutter/material.dart';

class Message extends StatefulWidget {
  final String type;
  final String name;
  final String msg;

  const Message({
    Key? key,
    required this.type,
    required this.name,
    required this.msg,
  }) : super(key: key);

  @override
  _MessageState createState() => _MessageState();
}

class _MessageState extends State<Message> {
  late Widget body;

  @override
  void initState() {
    super.initState();
    // log(widget.type);
    switch (widget.type) {
      case 'left':
        body = _MessageLeftState(name: widget.name, text: widget.msg);
        break;
      case 'right':
        body = _MessageRightState(name: widget.name, text: widget.msg);
        break;
      default:
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: body,
    );
  }
}

class _MessageLeftState extends StatelessWidget {
  final String name;
  final String text;
  const _MessageLeftState({
    Key? key,
    required this.name,
    required this.text,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Row(
          children: [
            FLAvatar(
              color: Color(0xFF0078D4),
              width: 50,
              height: 50,
              text: name.substring(0, 1),
              textStyle: TextStyle(fontSize: 17, color: Colors.white),
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.only(left: 15.0, bottom: 2.0),
                  child: Text(name),
                ),
                FLBubble(
                  from: FLBubbleFrom.left, // can change to top/bottom/right
                  backgroundColor: Colors.white,
                  child: Container(
                    // width: 300,
                    padding: EdgeInsets.symmetric(horizontal: 5, vertical: 8),
                    child: Text(
                      text,
                      style: TextStyle(fontSize: 15),
                      softWrap: true,
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

class _MessageRightState extends StatelessWidget {
  final String name;
  final String text;
  const _MessageRightState({
    Key? key,
    required this.name,
    required this.text,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                Padding(
                  padding: const EdgeInsets.only(right: 15.0, bottom: 2.0),
                  child: Text(name),
                ),
                FLBubble(
                  from: FLBubbleFrom.right, // can change to top/bottom/right
                  backgroundColor: Colors.green.shade300,
                  child: Container(
                    // width: 300,
                    padding:
                        const EdgeInsets.symmetric(horizontal: 5, vertical: 8),
                    child: Text(
                      text,
                      style: const TextStyle(
                          color: Color.fromARGB(255, 255, 255, 255),
                          fontSize: 15),
                      softWrap: true,
                    ),
                  ),
                ),
              ],
            ),
            FLAvatar(
              color: Color(0xFF5500D4),
              width: 50,
              height: 50,
              text: name.substring(0, 1),
              textStyle: TextStyle(fontSize: 17, color: Colors.white),
            ),
          ],
        ),
      ),
    );
  }
}
